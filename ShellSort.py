import random

def ShellSort(A, n):
    h = 1
    while 3*h+1 <= n/2:
        h = 3*h+1
    while h > 0:
        for k in range(0, h):
            i = k + h
            while i < n:
                x = A[i]
                j = i - h
                while j >= 0 and A[j] > x:
                    A[j+h] = A[j]
                    j -= h
                A[j+h] = x
                i += h
        h = h//3
    return A


#___________________________________________________________________________________
#Pseudocode simulation
def exch(A, index1, index2):
    A[index1], A[index2] = A[index2], A[index1]
    return A

#Array generation
def rand_intarray(size):
    output = []
    for item in range(size):
        output.append(random.randint(0,9))
    return output

#Generate array with unique numbers only
def rand_intarray_U(size):
    output = [number for number in range(size)]
    random.shuffle(output)
    return output

#Console output
if __name__ == '__main__':
    A = rand_intarray_U(50)
    print(A)
    A = ShellSort(A, len(A))
    print(A)
