#Not functional. Issue not found although code seems similar to source.

import random
def Left(i):
    return 2*i + 1
def Right(i):
    return 2*i + 2

def MaxHeapify(A, i, k):
    l = Left(i)
    r = Right(i)
    print(f"l: {l} r: {r}")

    #Pseudocode does not mind indexing out of bounds. Python does
    try:
        t = A[l] > A[i] > A[r]
    except (TypeError, IndexError):
        return A

    if l <= k and A[l] > A[i]:
        largest = l
    else:
        largest = i
    if r <= k and A[r] > A[largest]:
        largest = r
    if largest != i:
        A = exch(A, i, largest)
        A = MaxHeapify(A, largest, k)
    return A

def BuildMaxHeap(A, k):
    for i in range(k//2, -1, -1):
       print(i)
       A = MaxHeapify(A, i, k)
    return A


def HeapSort(A, n):
    A = BuildMaxHeap(A, n-1)
    k = n-1
    output = []
    while k > 0:
        A = exch(A, A[0], A[k])
        output.append(A[0])
        k -= 1
        A = MaxHeapify(A, 0, k)
    return output

#___________________________________________________________________________________
#Pseudocode simulation
def exch(A, index1, index2):
    A[index1], A[index2] = A[index2], A[index1]
    return A

#Array generation
def rand_intarray(size):
    output = []
    for item in range(size):
        output.append(random.randint(0,9))
    return output

#Generate array with unique numbers only
def rand_intarray_U(size):
    output = [number for number in range(size)]
    random.shuffle(output)
    return output

#Console output
if __name__ == '__main__':
    A = rand_intarray_U(7)
    print(A)
    A = HeapSort(A, len(A))
    print(A)