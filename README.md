# Sorting-algorithms-edu

Sorting algorithms for ALM, UPOL

The aim is to rewrite sorting algorithms from pseudocode in lesson materials
to Python, in order to have a runnable algorithm for testing, tinkering and
playing.

Currently, HeapSort, RadixSort and MergeSort are nonfunctional. See first comment of each
file for description.