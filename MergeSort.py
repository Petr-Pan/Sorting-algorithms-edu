#Non functional - diffs between pseudocode and Python too big to maintain resemblance

import random

def Merge(Al, Ar, l, q, r):
    i = l
    j = q+1
    k = 0
    #Merge halved arrays
    A = Al + Ar
    print(A)
    #Simulate static array in python
    B = [11,11,11,11,11,11,11,11,11,11]

    #if len(A) == 1:
    #    return A
    while i <= q and j <= r:
        #Debug line
        print(f"i: {i} and j: {j} l: {l} r: {r} q: {q}")
        if A[i] <= A[j]:
            B[k] = A[i]
            i += 1
            k += 1
        else:
            B[k] = A[j]
            j += 1
            k += 1
    while i <= q:
        B[k] = A[i]
        i += 1
        k += 1
    while j <= r:
        B[k] = A[j]
        j += 1
        k += 1
    return B

def MergeSort(A, l, r):
    if l < r:
        q = (l+r)//2
        #Split left and right half
        Al = A[:q]
        Ar = A[q:]
        #Sort and merge halves
        Al = MergeSort(Al, l, q-1)
        Ar = MergeSort(Ar, q+1, r)
        A = Merge(Al, Ar, l, q, r)
    return A


#___________________________________________________________________________________
#Pseudocode simulation
def exch(A, index1, index2):
    A[index1], A[index2] = A[index2], A[index1]
    return A

#Array generation
def rand_intarray(size):
    output = []
    for item in range(size):
        output.append(random.randint(0,9))
    return output

#Generate array with unique numbers only
def rand_intarray_U(size):
    output = [number for number in range(size)]
    random.shuffle(output)
    return output

#Console output
if __name__ == '__main__':
    A = rand_intarray_U(10)
    print(A)
    A = MergeSort(A, 0, len(A)-1)
    print(A)