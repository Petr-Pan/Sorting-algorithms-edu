#Non functional, although similar to source

import random

def RadixSort(A, n):
    digits_in_number = 1
    numeral_system = 10
    #Static array simulation
    B = [11 for i in range(numeral_system)]
    for d in range(digits_in_number):
        C = []
        for i in range(numeral_system):
            C.append(0)
        print("Empty C field\n", C)
        for i in range(0, n-1):
            j = Cislice(A[i], d)
            C[j+1] = C[j+1] + 1
        print("C field with sizes of buckets\n", C)
        for i in range(2,numeral_system-1):
            C[i] = C[i] + C[i-1]
        print("C field with indexes of starts of buckets\n", C)
        for i in range(0, n-1):
            j = Cislice(A[i], d)
            B[C[j]] = A[i]
            C[j] = C[j] + 1
        for i in range(0, n-1):
            A[i] = B[i]
    return A



#def Cislice(cislo, rad):
#    """Returns number on a given place, indexing starts on right"""
#    return cislo // 10**rad % 10

def Cislice(cislo, rad):
    """Returns number on a given place, indexing starts on left"""
    stringed = str(cislo)
    return int(stringed[rad])

#___________________________________________________________________________________
#Pseudocode simulation
def exch(A, index1, index2):
    A[index1], A[index2] = A[index2], A[index1]
    return A

#Array generation
def rand_intarray(size):
    output = []
    for item in range(size):
        output.append(random.randint(0,9))
    return output

#Generate array with unique numbers only
def rand_intarray_U(size):
    output = [number for number in range(size)]
    random.shuffle(output)
    return output

#Console output
if __name__ == '__main__':
    A = rand_intarray(8)
    print(A)
    A = RadixSort(A, len(A))
    print(A)
